from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

# TODO : déplacer dans une classe à part
class Mot(BaseModel):
    id:int
    caracteres:str
    
# TODO : déplacer getMots dans un autre fichier que le main
def getMots():
    return ["arbre","maison","voiture"]

@app.get("/mots")
def read_root():
    return {"mots":getMots()}

@app.post("/mot")
def add_mot(mot: Mot):
    print(mot.caracteres)



@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}